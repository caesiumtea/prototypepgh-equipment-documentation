# Welcome to PrototypePGH

Prototype is a space to make things, fail quickly, and find the support to start all over again.

## About this Project

This repository is the place where we store documentation for the various equipment in the workspace.

It was inspired by this blue sticky! --> <img src='./IMG_2312.JPG' height='10%' width='10%'>

You can check out the project charter [here](https://docs.google.com/document/d/1Pc7oqLpJ6ijRbNUJ6rarQYdKa_GPyhlHz_R1BQ3vpP4/edit?usp=sharing)

You should be able to find: 
- Operation Guides
- Maintenance Guides
- Training/Demonstrational Guides

... for each piece of equipment!

Happy Prototyping!


## Table of Contents

1. [Full Spectrum CO2 Laser Cutter](./equipment-documentation/laser-cutter/OperationManual-Laser%20Cutter.md)
2. [Elegoo 3D resin printer](./equipment-documentation/3d-resin-printer/OperationManual%20-%203D%20Resin%20Printer.md)
3. [CNC vinyl cutter](./equipment-documentation/cnc-vinyl-cutter/OperationManual%20-%20Vinyl%20Cutter.md)
4. [Arduino boards](./equipment-documentation/arduino-boards/OperationManual%20-%20Arduino%20Boards.md)
5. [Screen printing equipment](./equipment-documentation/screen-printing-equipment/OperationManual%20-%20Screen%20Printing%20Equipment.md)
6. [Button maker](./equipment-documentation/button-maker/OperationManual%20-%20Button%20Maker.md)
7. [Brother laser printer](./equipment-documentation/brother-laser-printer/OperationManual%20-%20Brother%20Laser%20Printer.md)
8. [Hat press and t-shirt heat press](./equipment-documentation/hat-press-heat-press/OperationManual%20-%20T-Shirt%20Heat%20Press%20and%20Hat%20Press.md)

## Contributing

[Send Kyle a slack message] to learn more about how you can contribute.

You'll need:
- A gitlab account (you can sign in using GitHUB account if you want)
- Write access to this repo
- A text editor (Kyle's pick => [VSCode](https://code.visualstudio.com/download))
- [Git](https://git-scm.com/downloads)