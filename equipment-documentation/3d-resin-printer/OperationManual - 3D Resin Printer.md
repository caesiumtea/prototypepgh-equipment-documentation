# Operation Manual: 3d Resin Printer

This document will outline the process to use the 3d Resin Printer.
<img src="../../qrcodes/3d-resin-printer.png" style="background-color: white; display:block" >
## Training Requirements
```
NOTE: THIS EQUIPMENT REQUIRES TRAINING VIA ON IN-PERSON EVENT.
SIGN UP FOR A TRAINING ON THE [EVENTS PORTION OF PROTOTYPEPGH.COM](https://prototypepgh.com/events)
```

----------

## Operations

### Walkthrough Video

```
----Coming Soon -----
For a Video Of this Process
```

### Saftey 
- DO NOT pour resin PAST the "max" line
- You MUST wear gloves and a mask while using the Elgoo resin printer. The resin can burn skin and irritate eyes.
- It's a good idea to open windows while using the 3D printer.

### Before You Start

TODO: tips, tricks or other things that will make the steps ahead easier.
(If you wanna contribute we'd love to have your insight)

### Using the Equipment

#### Overview of FDM vs SLA 3D Printing

- FDM (Fused Deposition Modeling) uses a thermoplastic polymer in a filament form (the PrintrBot uses FDM)
- SLA (Stereolitography Apparatus) uses a liquid resin to print objects. A Laser lists at the bottom of the vat filled with liquid resin and then flashes onto the photosensitive resin. The exposure to the laser cures (hardens) the material (the Elgoo uses SLA)
- Pros to FDM - filament is cheaper; many colors to choose from ; better for durable larger long lasting pars; filament doesn't smell.
- Cons to FDM - the surface finish is coars and the layers are visible; not good for small features.
- Pros to SLA - much smoother surface finsih; better for smaller and more detailed figures; better for delicate and dentailed parts.
- Cons to SLA - resin is more expensive; limited colors like black, gray , transparent, blue; resin is toxic and requires gloves/masks.

#### Designing files

- Fusion360 (free account for student/hobbyist)
- Thingiverse has free open-source 3D print files
- TinkerCAD is a tool marketed towards children but is a free online tool.
- Instructables has laser utting and 3D printing files that are mostly free and open source.

#### Sending Files to the Elegoo Printer

- Chitubox is a slicking software that is downloaded into the Black Laptop next to the Vinyl Cutter (See `insert-password-manager`)- You don't use Chitubox to design files, only to conver from .STL (3D models) to a format that Elegoo needs (`.cbddlp`)
- You also use Chitubox to add any supports to the print; if there are large overhanging parts you need to add supports which you will then break off at the end of the print. 
- You can try to print multiple objects at once; cons -> the print time is longer and if the print fails you loos all objects.
- And save your `.cbddlp` file onto the USB Flash drive (slice then save as `.cbddlp`)
- Safely eject the USB by clicking on the "left arrow key" in the bottom right corner of the laptop screen and choosing "safely eject USB"

#### Turn the 3D Printer on and insert the USB key

- Once your file si ready and saved to the USB key, turn the printer on (button on the lower, back of the printer) and insert the USB key. 
- Chose "Print" on the Elegoo touch scren and you will see all of the files saved to the USB Key.
- choose the file you want to print and you will see a time estimation on the bottom of the Elegoo screen for how long it wil take until the print is done. 
- Once the print is done (usually 1-2hrs for small prints) use the plastic scraper to remove your print from the build platform. 
- Clean your print with alcohol and paper towels
- Pur the rest of the resin back into the resin bottle by funneling it through the paper funnel; do not use resin without filtering it first.
- clean the build platform and resin tank with alcohol and paper towels after ever print. 

1. 
2. 
3. 
4. 
5. 

### Edit this document

This document is hosted on gitlab.com and is currently being maintained by (Kyle Bennett: @BennEntterprise) and/or the Operations Committee at Large. 

If you think this document can benefit from improvement, submit a PULL REQUEST or contact the maintainer.

Other contributors include:
- (if you make an edit or suggestion put your name here to get credit!)
