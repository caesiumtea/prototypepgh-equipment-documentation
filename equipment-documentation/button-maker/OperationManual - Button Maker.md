# Operation Manual: Button Maker

This document will outline the process to use the Button Maker
<img src="../../qrcodes/button-maker.png" style="background-color: white; display:block" >

## Training Requirements

```
NOTE: THIS EQUIPMENT REQUIRES TRAINING VIA ON IN-PERSON EVENT.
SIGN UP FOR A TRAINING ON THE [EVENTS PORTION OF PROTOTYPEPGH.COM](https://prototypepgh.com/events)
```

----------

## Operations

### Walkthrough Video

```
----Coming Soon -----
For a Video Of this Process
```

### Before You Start

*tips, tricks or other things that will make the steps ahead easier.


### Using the Equipment

Steps For Use: 

1. 
2. 
3. 
4. 
5. 

### Edit this document

This document is hosted on gitlab.com and is currently being maintained by (Kyle Bennett: @BennEntterprise) and/or the Operations Committee at Large. 

If you think this document can benefit from improvement, submit a PULL REQUEST or contact the maintainer.

Other contributors include:
- (if you make an edit or suggestion put your name here to get credit!)
