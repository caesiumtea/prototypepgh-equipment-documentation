# Operation Manual: Laser Cutter

This document will outline the process to use the Laser Printer
<img src="../../qrcodes/laser-cutter.png" style="background-color: white; display:block" />

## Training Requirements

```
NOTE: THIS EQUIPMENT REQUIRES TRAINING VIA ON IN-PERSON EVENT.
SIGN UP FOR A TRAINING ON THE [EVENTS PORTION OF PrototypePgh.com](https://prototypepgh.com/events)
```

----------

## Operations

### Walk-through Video

```
----Coming Soon -----
For a Video Of this Process
```

#### Safety Rules: 

> 1. In the case of an emergency, press the RED E-STOP button in the bottom left hand corner of the laser cutter (also the top right corner of the Retina Engrave Software)
> 2. A CO2 fire extinguisher is on the table next to the laser cutter but this should only be used as a last resort if the fire continues.
> 3. DO NOT LEAVE THE LASER UNATTENDED WHILE IN USE.


### Before You Start

*tips, tricks or other things that will make the steps ahead easier.


### Using the Equipment

#### Rules

>1. You must have been trained to use this equipment. If you need training, check the [PrototypePGH >Calendar]() and attend the training.
>2. Reserve time to be working on the Laser Cutter (even if no one is around). Add up to 2hrs to the [Laser Cutter Signup](). This helps make sure everyone can work uninterrupted, and so that the operations subcommittee can keep up on maintenance and understand how often the machine is used.
>3. DO NOT use any material other than cast acrylic, wood, cardboard, and cardstock paper in the Prototype LAser Printer. Wood and acrulic are available for purchase, [see the menu and purchase via Venmo]()
    - Wood $5
    - Acrylic $10
>4. IF YOU USE MIRRORED ACRYLIC you must leave the paper coating on it. A reflective surface can cause the laser to reflect (and DAMAGE) the equipment.
>5. DO NOT BRING VINYL RECORDS AS THE EMIT TOXIC FUMES WHILE BEING CUT.
>6. The largest material size that can be used is 11 x 19 x 1/4 inches.
>6. You must use CHROME to run the [Rentina Engrave Software](https://laser101.fslaser.com/RetinaEngrave).  
>7. If you have any issues while using the laser cutter please email hello@prototypepgh.com, etc1073@gmail.com or message the Protobabe Signal group for advice.
>8. You must VACCUU/DUST the laser bed after use.

#### Instructions

1. Turn on laser cutter (red button on back of laser cutter)
2. Turn on the cooling system (black button on the cord in the back of the cooling box)
3. Connect laser cutter via ethernet cable (blue cable) to laptop (either the provided laptop or your personal laptop)
4. Open a chrome browser and type this IP Address in the search bar:
    169.254.146.181/designer
This will open the browser based Retina Engrave Design Software, which is free to use (the computer must be physically connected to the laser in order for the software to work.)
5. Open the lid and place your material on the bed.
6. Focust the laser on the center of your material by placing the 1/4th " hockey puck on your material and then manually unscrewing the laser head and moving it up or down, tighten the sscrew wehn the laser head rests on the hockey puck.
7. misnumberd
8. Set reference points by choosing "reference markers" on the touch screen and then use the arrows to move the laser to the desired area or starting point.
9. Press "Set marker" or "Set Auto Marker" and you can see the dots on the screen as the laser moves around - you can use these as guides to where to place your design on the bed to be cut/engraved.
10. To set the power and speed of the laser, make sure that you object is not currently selected (click on the white grid space around the object) then input the speed and power for our material:
    - 1/8th inch cas acrylic (without paper coating) ====> 30% speed 100% power
    - 1/8th Wood ====> 30% speed 100% power
    - Cardstock/Cardboard/Paper ====> 100% Speed 10% Power
11. To etch or engrave (instead of cut) you can drag and drop an image from the computer into the software and it will immediately resterize the image (a raser is made up of black and grey pixes which the laser cutter translates to etching)
12. 12. A Cevtor is made up of red lines and curves which the laser cutter translates to cutting - you can import a silhouette image into the software and "trace image" to conver it to a cut line. 
13. The Retina Engrage software will give a time estimate of how long the cut or etch will take, at the bottom of the screen, and it shows the progress as well.
14. When you are done using the laser cutter, please remember to turn off the laser cutter and the cooling system, and vacuume the laser bed to clean up dust and small pieces of material.

#### Tips 
When working with the software
 - Red lines = Cut lines 
 - Black Lines/Shapes = Engraving areas 
# Other Useful Links
- [Manufacture Instruction Booklet (106pgs)](https://info.fslaser.com/hubfs/Public_Documents/Muse%20Manual.pdf)
- [How to Use a Fire Extinguisher - Carbon Dioxide (CO2)](https://youtu.be/i4HrH8QpdIM)


### Edit this document

This document is hosted on gitlab.com and is currently being maintained by (Kyle Bennett: @BennEntterprise) and/or the Operations Committee at Large. 

If you think this document can benefit from improvement, submit a PULL REQUEST or contact the maintainer.

Other contributors include:
- (if you make an edit or suggestion put your name here to get credit!)
