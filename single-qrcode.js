var QRCode = require("qrcode");

// Grab the arguments passed on command line
var args = process.argv.slice(2);

// Grab the first argument as a filename string
var filename = args[0];

// Grab the second argument as a URL string
var url = args[1];

// Validate there are 2 arguments
if (args.length != 2) {
    console.log("Usage: node qrcode.js <filename> <url>");
    process.exit(1);
}

QRCode.toFile(filename, url, {
    color: {
        dark: '#000',  // Blue dots
        light: '#fff' // Transparent background
    }
}, function (err) {
    if (err) throw err
    console.log('done')
})