// This file wil read the url-links.txt file and generate a QR code for each line in the file.
// The data is stored as "equipment_name target_url"

/// Import the required modules
const fs = require('fs'); // The File System. So we can read/write operations.
const QRCode = require('qrcode'); // A Package to make QR Codes https://www.npmjs.com/package/qrcode
// You can check out the security of that package here: https://socket.dev/npm/package/qrcode

/// Read the file and split it into an array,
//  and then loop through the array,
//  also handles any errors.
fs.readFile('url-links.txt', 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    try {
        // If there is not a qrcodes directory, create one
        if (!fs.existsSync('qrcodes')) {
            fs.mkdirSync('qrcodes');
        }


        // NOTE: Start with i=1 to skip the first line, which has our headings
	// HEADING: equipment_name target_url
        var lines = data.split('\n');
        for (var i = 1; i < lines.length; i++) {
            var line = lines[i];
            var parts = line.split(' ');
        
	    // put the file in a subdirectory called qrcodes
            var filename = 'qrcodes/' + parts[0] + '.png';
            var url = parts[1];
            QRCode.toFile(filename, url, {
                color: {
                    dark: '#000', // Black dots
                    light: '#fff' // White background
                }
            }, function (err) {
                if (err) {
                    console.log('Exiting with a non-zero code. ERR!')
                    throw err
                }
            })
        }
    } catch (error) {
        return console.log(error);
    }
});
